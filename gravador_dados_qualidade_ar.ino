/*
UFPB, Engenharia Ambiental 2019.2 

Projeto : Dispositivo feito com Arduino Nano e sensore MQ2, MQ7 e MQ135, para calcular os níveis dos gases poluentes, em ppm, no ar atmosférico da cidade de João Pessoa. 

Artigo: Monitoramento da qualidade do ar por meio de sensoriamento remoto

Alunos : 
JEFFERSON LUIZ - Matrícula: 20190XXXX70
GIULLIANA GOMES - Matrícula: 20190XXXX97
PEDRO ANDRÉ - Matrícula: 20190XXXX36
TUANY KEROLAYNE - Matrícula: 20190XXXX52

MQ-2
– Detecção de fumaça e gases inflamáveis: GLP, Metano, Propano, Butano, Hidrogênio, Álcool, Gás Natural e outros inflamáveis.
www.haoyuelectronics.com/Attachment/MQ-2/MQ-2.pdf

MQ-7
– Detecção do gás Monóxido de Carbono
https://www.sparkfun.com/datasheets/Sensors/Biometric/MQ-7.pdf

MQ-135
– Detecção de gases tóxicos como amônia, dióxido de carbono, benzeno, óxido nítrico, fumaça e álcool
https://www.olimex.com/Products/Components/Sensors/SNS-MQ135/resources/SNS-MQ135.pdf

Outras referências

Sensoriamento de gases em tempo real através de sensores MQ-(0-9)
https://jualabs.wordpress.com/2016/11/30/sensoriamento-de-gases-em-tempo-real-atraves-de-sensores-mq-0-9/

MQ gas sensor correlation function estimation:
http://davidegironi.blogspot.com/2017/05/mq-gas-sensor-correlation-function.html#.Xi-FjyNv-Ul

MQ135 - Arduino library for the MQ135
https://github.com/smilexth/MQ135

MQ7-Library - CO (Carbon Monoxide) concentration from MQ7 sensor
https://github.com/swatish17/MQ7-Library

MQ-2 sensor library
https://github.com/yanoschik/MQ2Lib

Inexpensive data logging - projeto de data logger com microSD
https://publiclab.org/notes/cfastie/04-25-2017/inexpensive-data-logging

Cutting Power to Secure Digital Media cards for Low Current Data Logging
https://thecavepearlproject.org/2017/05/21/switching-off-sd-cards-for-low-power-data-logging/
*/

#include "Arduino.h"
#include "SPI.h"
#include "SD.h"
#include "MQ135.h"
#include "MQ7.h"
#include "MQ2Lib.h"
 
#define pinoAnalogicoMQ2 A0
#define pinoAnalogicoMQ7 A1
#define pinoAnalogicoMQ135 A2

#define pinoSom 6
#define pinoLedErro 7
#define pinoLedSD 8

#define pinoModuloSD 10
#define pinoMOSI 11
#define pinoMISO 12
#define pinoSCK 13

#define transistorMQ2 2
#define transistorMQ7 3
#define transistorMQ135 4

int valorAnalogMQ2;
int valorAnalogMQ7;
int valorAnalogMQ135;

long anteriorSegundoMillis = 0;
long intervaloSegundoMillis = 1000;

long anteriorMinutoMillis = 0;
long intervaloMinutoMillis = 60000;

int segundoLeitura = 0;

int indice = 0;

int indiceLeitura = 0;

int indiceArquivo = 0;

String registroLeitura = "";
 
int segundo = 0;
int minuto = 0;
int hora = 0;

char horario[] = "00:00:00";

File arquivoCSV;

#define RZERO 206.85 //  Valor de calibracao
MQ135 gasSensor = MQ135(pinoAnalogicoMQ135);

MQ7 mq7(pinoAnalogicoMQ7, 5.0);

MQ2 mq2(pinoAnalogicoMQ2, false);

float co2[3] = {0, 0, 0};
float glp[3] = {0, 0, 0};
float coMQ2[3] = {0, 0, 0};
float coMQ7[3] = {0, 0, 0};
float fumaca[3] = {0, 0, 0};

int leituraSegundo[3] = {0, 0, 0};
int leituraMinuto[3] = {0, 0, 0};
int leituraHora[3] = {0, 0, 0};

void setup() {
   Serial.begin(9600);

   pinMode(pinoSom, OUTPUT);
   pinMode(pinoLedErro, OUTPUT);
   pinMode(pinoLedSD, OUTPUT);
   
   pinMode(pinoAnalogicoMQ2, INPUT);
   pinMode(pinoAnalogicoMQ7, INPUT);
   pinMode(pinoAnalogicoMQ135, INPUT);
   
   pinMode(transistorMQ2, OUTPUT);
   pinMode(transistorMQ7, OUTPUT);
   pinMode(transistorMQ135, OUTPUT);

   // Liga os sensores para inicializacao
   digitalWrite(transistorMQ2, HIGH);
   digitalWrite(transistorMQ7, HIGH);
   digitalWrite(transistorMQ135, HIGH);

   // Liga o modulo do cartao de memoria
   digitalWrite(pinoLedSD, HIGH);

   delay(1000);
   
   if (!SD.begin(pinoModuloSD)) {
     Serial.println(F("Falha ao inicializar SD."));
     
     tone(pinoSom, 1000, 500);
     
     digitalWrite(pinoLedErro, HIGH);
   } else {
     while (SD.exists("dados" + String(indiceArquivo) + ".csv")) {
       indiceArquivo++;     
     }
     
     Serial.println("Arquivo: dados" + String(indiceArquivo) + ".csv");
     
     arquivoCSV = SD.open("dados" + String(indiceArquivo)  + ".csv", FILE_WRITE);
   
     if (arquivoCSV) {
        // Grava cabecalho do arquivo de dados
        arquivoCSV.println("hora;minuto;segundo;horario;co2;co_mq7;co_mq2;glp;fumaca");
        
        arquivoCSV.close();

        digitalWrite(pinoLedErro, LOW);
     } else {
        Serial.println(F("Erro ao abrir arquivo."));
        
        tone(pinoSom, 1000, 500);

        digitalWrite(pinoLedErro, HIGH);
     }
   }
   
   Serial.println("Inicializando sensores...");

   delay(59000);
   
   mq2.begin();
   
   float rzero = gasSensor.getRZero();
   
   Serial.print("MQ135 RZERO Calibration Value : ");
   Serial.println(rzero);
   
   digitalWrite(transistorMQ2, LOW);
   digitalWrite(transistorMQ7, LOW);
   digitalWrite(transistorMQ135, LOW);
   digitalWrite(pinoLedSD, LOW);
}
 
void loop() {
  unsigned long atualMillis = millis();
  
  if (atualMillis - anteriorSegundoMillis > intervaloSegundoMillis) {
    anteriorSegundoMillis = atualMillis;

    sprintf(horario, "%02d:%02d:%02d", hora, minuto, segundo);

    Serial.println(horario);
    
    segundo++;

    segundoLeitura++;

    switch (segundoLeitura) {
      case 60:
        // Liga os sensores
        Serial.println("Aquecendo sensores");
        
        digitalWrite(transistorMQ2, HIGH);
        digitalWrite(transistorMQ7, HIGH);
        digitalWrite(transistorMQ135, HIGH);
      break;
      
      case 100:
        // Primeira leitura dos dados dos sensores
        valorAnalogMQ2 = analogRead(pinoAnalogicoMQ2); 
        valorAnalogMQ7 = analogRead(pinoAnalogicoMQ7); 
        valorAnalogMQ135 = analogRead(pinoAnalogicoMQ135);

        leituraHora[indiceLeitura] = hora;
        leituraMinuto[indiceLeitura] = minuto;
        leituraSegundo[indiceLeitura] = segundo;

        co2[indiceLeitura] = gasSensor.getPPM();
        coMQ7[indiceLeitura] = mq7.getPPM();
        coMQ2[indiceLeitura] = mq2.readCO();
        glp[indiceLeitura] = mq2.readLPG();
        fumaca[indiceLeitura] = mq2.readSmoke();

        Serial.print("1a leitura = ");
        Serial.print("co2: ");
        Serial.print(co2[indiceLeitura]);
        Serial.print(", coMQ7: ");
        Serial.print(coMQ7[indiceLeitura]);
        Serial.print(", coMQ2: ");
        Serial.print(coMQ2[indiceLeitura]);
        Serial.print(", glp: ");
        Serial.print(glp[indiceLeitura]);
        Serial.print(", fumaca: ");
        Serial.println(fumaca[indiceLeitura]);

        indiceLeitura++;
      break;
      
      case 110:
        // Segunda leitura dos dados dos sensores
        valorAnalogMQ2 = analogRead(pinoAnalogicoMQ2); 
        valorAnalogMQ7 = analogRead(pinoAnalogicoMQ7); 
        valorAnalogMQ135 = analogRead(pinoAnalogicoMQ135);
        
        leituraHora[indiceLeitura] = hora;
        leituraMinuto[indiceLeitura] = minuto;
        leituraSegundo[indiceLeitura] = segundo;

        co2[indiceLeitura] = gasSensor.getPPM();
        coMQ7[indiceLeitura] = mq7.getPPM();
        coMQ2[indiceLeitura] = mq2.readCO();
        glp[indiceLeitura] = mq2.readLPG();
        fumaca[indiceLeitura] = mq2.readSmoke();

        Serial.print("2a leitura = ");
        Serial.print("co2: ");
        Serial.print(co2[indiceLeitura]);
        Serial.print(", coMQ7: ");
        Serial.print(coMQ7[indiceLeitura]);
        Serial.print(", coMQ2: ");
        Serial.print(coMQ2[indiceLeitura]);
        Serial.print(", glp: ");
        Serial.print(glp[indiceLeitura]);
        Serial.print(", fumaca: ");
        Serial.println(fumaca[indiceLeitura]);

        indiceLeitura++;
      break;
      
      case 120:
        // Terceira leitura dos dados dos sensores
        valorAnalogMQ2 = analogRead(pinoAnalogicoMQ2); 
        valorAnalogMQ7 = analogRead(pinoAnalogicoMQ7); 
        valorAnalogMQ135 = analogRead(pinoAnalogicoMQ135);
        
        leituraHora[indiceLeitura] = hora;
        leituraMinuto[indiceLeitura] = minuto;
        leituraSegundo[indiceLeitura] = segundo;

        co2[indiceLeitura] = gasSensor.getPPM();
        coMQ7[indiceLeitura] = mq7.getPPM();
        coMQ2[indiceLeitura] = mq2.readCO();
        glp[indiceLeitura] = mq2.readLPG();
        fumaca[indiceLeitura] = mq2.readSmoke();

        Serial.print("3a leitura = ");
        Serial.print("co2: ");
        Serial.print(co2[indiceLeitura]);
        Serial.print(", coMQ7: ");
        Serial.print(coMQ7[indiceLeitura]);
        Serial.print(", coMQ2: ");
        Serial.print(coMQ2[indiceLeitura]);
        Serial.print(", glp: ");
        Serial.print(glp[indiceLeitura]);
        Serial.print(", fumaca: ");
        Serial.println(fumaca[indiceLeitura]);

        indiceLeitura++;
        
        // Desliga os sensores
        Serial.println("Desligando sensores");
        
        digitalWrite(transistorMQ2, LOW);
        digitalWrite(transistorMQ7, LOW);
        digitalWrite(transistorMQ135, LOW);

        segundoLeitura = 0;
      break;
    }
    
    if (segundo == 60) {
      segundo = 0;
      
      minuto++;

      if (minuto == 60) {
        minuto = 0;

        hora++;

        if (hora == 24) {
          hora = 0;
        }
      }
    }
    
    switch (indiceLeitura) {
      case 2:
        // Liga o modulo microSD
        digitalWrite(pinoLedSD, HIGH);
      break;
      
      case 3:
        // Grava dados no cartao de memoria
        Serial.println("Gravando dados no cartao de memoria");

        arquivoCSV = SD.open("dados" + String(indiceArquivo)  + ".csv", FILE_WRITE);

        if (arquivoCSV) {
          for (int indice = 0; indice < 3; indice++) {
            registroLeitura = "";

            registroLeitura.concat(leituraHora[indice]);
            registroLeitura.concat(";");
            registroLeitura.concat(leituraMinuto[indice]);
            registroLeitura.concat(";");
            registroLeitura.concat(leituraSegundo[indice]);
            registroLeitura.concat(";;");
            registroLeitura.concat(co2[indice]);
            registroLeitura.concat(";");
            registroLeitura.concat(coMQ7[indice]);
            registroLeitura.concat(";");
            registroLeitura.concat(coMQ2[indice]);
            registroLeitura.concat(";");
            registroLeitura.concat(glp[indice]);
            registroLeitura.concat(";");
            registroLeitura.concat(fumaca[indice]);
            
            registroLeitura.replace(".", ",");
            
            arquivoCSV.println(registroLeitura);
          }
        
          arquivoCSV.close();

          digitalWrite(pinoLedErro, LOW);
        } else {
          Serial.println(F("Erro ao abrir arquivo."));

          tone(pinoSom, 1000, 500);

          digitalWrite(pinoLedErro, HIGH);
        }
          
        indiceLeitura = 0;

        digitalWrite(pinoLedSD, LOW);
      break;
    }
  }
}
